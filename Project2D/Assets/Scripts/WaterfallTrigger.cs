﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterfallTrigger : MonoBehaviour
{
    public AudioSource audioSource;
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && !audioSource.isPlaying)
        {
            audioSource.Play();
        }
            
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player" && !audioSource.isPlaying)
        {
            audioSource.Stop();
        }

    }
}
